#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
from data.services.Arquivo import Arquivo
from data.services.Imagem import Imagem
from data.services.Listar import Listar
from data.util.Locais import Locais


class Projeto(object):
    """Classe responsável por realizar toda a manipulação dos projetos"""

    ##########################################################################################
    # Init
    ##########################################################################################
    def __init__(self):
        """Construtor"""

        self._nome_projeto = None
        self._imagens_positivas = None
        self._imagens_negativas = None
        self._filtros_imagens = list()
        self._tamanho_maximo = 0
        self._imagens_teste = None
        self._video_teste = None

        self._arquivos = Arquivo()
        self._imagens = Imagem()

    ##########################################################################################
    # Gets e Sets
    ##########################################################################################

    def set_nome_projeto(self, nome_projeto):
        self._nome_projeto = nome_projeto
        return self._verificar_nome_projeto(nome_projeto)

    def get_nome_projeto(self):
        return self._nome_projeto

    def set_imagens_positivas(self, imagens_positivas):
        self._imagens_positivas = imagens_positivas

    def get_imagens_positivas(self):
        return self._imagens_positivas

    def set_imagens_negativas(self, imagens_negativas):
        self._imagens_negativas = imagens_negativas

    def get_imagens_negativas(self):
        return self._imagens_negativas

    def set_filtros_imagens(self, filtro_imagens):
        self._filtros_imagens.append(filtro_imagens)

    def get_filtros_imagens(self):
        return self._filtros_imagens

    def set_tamanho_maximo(self, tamanho_maximo):
        self._tamanho_maximo = tamanho_maximo

    def get_tamanho_maximo(self):
        return self._tamanho_maximo

    def set_imagens_teste(self, imagens_teste):
        self._imagens_teste = imagens_teste

    def get_imagens_teste(self):
        return self._imagens_teste    

    def set_video_teste(self, video_teste):
        self._video_teste = video_teste

    def get_video_teste(self):
        return self._video_teste

    ##########################################################################################
    # Verificar
    ##########################################################################################
    @staticmethod
    def _verificar_nome_projeto(nome_projeto):
        """Verifica se o projeto existe

            :param str nome_projeto: define o nome do projeto
        """
        
        projetos = Listar().listar_projetos()
        
        for p in range(len(projetos)):
            if projetos[p] == nome_projeto:
                return False
        
        return True

    ##########################################################################################
    # Manipula Projeto
    ##########################################################################################

    @property
    def criar_projeto(self):
        """Cria o projeto e suas estruturas"""
        
        # Cria a estrutura de pastas
        pasta_projeto = Locais().pasta_projetos()
        
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "cascade"))
        open(os.path.join(pasta_projeto, self.get_nome_projeto(), "cascade", "listaCascade.txt"), "w")
        
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "negativas", "editadas", "cinza"))
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "negativas", "editadas", "histograma"))
        
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "negativas", "originais"))
        
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "positivas", "editadas", "cinza"))
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "positivas", "editadas", "histograma"))
        
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "positivas", "originais"))
        
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "teste", "imagens", "detectadas"))
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "teste", "imagens", "editadas"))
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "teste", "imagens", "originais"))
        
        os.makedirs(os.path.join(pasta_projeto, self.get_nome_projeto(), "teste", "videos"))

        # Copia as imagens
        self._arquivos.copiar_imagens(self.get_nome_projeto(), "p", self.get_imagens_positivas())
        self._arquivos.copiar_imagens(self.get_nome_projeto(), "n", self.get_imagens_negativas())
                
        # Redimensiona as imagens CINZA
        self._imagens.redimensionar(self.get_nome_projeto(), "p", self.get_tamanho_maximo())
        self._imagens.redimensionar(self.get_nome_projeto(), "n", self.get_tamanho_maximo())
        
        # Aplica filtros
        self._imagens.aplicar_filtro(self.get_nome_projeto(), "p", self.get_filtros_imagens())
        self._imagens.aplicar_filtro(self.get_nome_projeto(), "n", self.get_filtros_imagens())
        
        # Gera as listas das imagens (.txt)
        Arquivo().arquivo_lista(self.get_nome_projeto(), self.get_filtros_imagens())
        
        return True

    @staticmethod
    def listar_projetos():
        """Retorna a lista dos projetos"""
        return Listar().listar_projetos()

    @staticmethod
    def apagar_projeto(nome_projeto):
        """Apaga o projeto"""
        Arquivo().apagar(nome_projeto)
