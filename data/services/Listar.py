#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
from data.util.Locais import Locais


class Listar(object):
    """Classe responsavel por gerar listas de arquivos e pastas e as retornar"""

    ##########################################################################################
    # Listar Projetos
    ##########################################################################################
    @staticmethod
    def listar_projetos():
        """Lista os projetos dentro do sistemas e retorna uma lista contendo seus nomes"""
        
        lista_projetos = os.listdir(Locais().pasta_projetos())
        
        projetos = list()

        for i in range(len(lista_projetos)):
            projetos.append(lista_projetos[i])

        return projetos

    ##########################################################################################
    # Listar Imagens
    ##########################################################################################
    @staticmethod
    def listar_imagens(local):
        """Lista as Imagens de um determinado local (path) e retorna a lista do path absoluto de cada imagem"""
                
        caminhos = [os.path.join(os.path.dirname(__file__), local, nome) for nome in os.listdir(local)]
        imagens = [arq for arq in caminhos if os.path.isfile(arq)]
        
        return imagens

    ##########################################################################################
    # Listar Treinamentos
    ##########################################################################################
    @staticmethod
    def listar_treinamentos(nome_projeto):
        """Abre o arquivo listaCascade.txt de um projeto expecifico e retorna a lista dos treinos realizados"""
        
        local = os.path.join(Locais().pasta_cascade(nome_projeto), "listaCascade.txt")
        
        return [line.rstrip('\n') for line in open(local, 'r')]
