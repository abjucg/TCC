#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import xml.etree.ElementTree as et
from data.util.Locais import Locais


def ler_xml(nome_projeto, cascade):
    """ Método responsável realizar a leitura das configurações utilizadas em um arquivo cascade

        :param nome_projeto: nome do projeto
        :type nome_projeto: str

        :param cascade: local em que se localiza o arquivo cascade do projeto selecionado
        :type cascade: str(path)

        :return: parâmetros do arquivo cascade selecionado
    """
    
    xml_file = os.path.join(Locais().pasta_cascade(nome_projeto), cascade, 'params.xml')
    
    # Carrega o xml para a memoria
    tree = et.parse(xml_file)
        
    root = tree.getroot()
    
    parametros = list()
    
    for child in root:
        for element in child:
            if element.tag != 'featureParams' and element.tag != 'stageParams':
                parametros.append([element.tag, element.text])
            else:
                for subelement in element:
                    parametros.append([subelement.tag, subelement.text])

    return parametros
