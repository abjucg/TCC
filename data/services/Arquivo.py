#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import shutil
from PIL import Image
from data.services.Listar import Listar
from data.services.Imagem import Imagem
from data.util.Codigos import verifica_codigo
from data.util.Locais import Locais


class Arquivo(object):
    """Classe responsável por oferecer os serviços relacionados a manipulação de arquivos dos proejtos
    como, copiar imagens e apagar arquivos"""

    ##########################################################################################
    # Copiar Imagens
    ##########################################################################################
    @staticmethod
    def copiar_imagens(nome_projeto, tipo_imagem, origem):
        """Copia as imagens de um local para a parta "originais" de um projeto expecifico
        
            :param nome_projeto: Nome do Projeto
            :type nome_projeto: str

            :param tipo_imagem: Tipo de Imagem (ver códigos de imagens)
            :type tipo_imagem: str

            :param origem: Local onde as imagens estão
            :type origem: str
        """

        lista_imagens = Listar().listar_imagens(origem)

        codigo_tipo = verifica_codigo(tipo_imagem)
        
        if codigo_tipo == "teste":
            pasta_originais = Locais().pasta_teste_imagens_originais(nome_projeto)
            
        elif codigo_tipo == "negativas":
            pasta_originais = Locais().pasta_imagens_negativas_originais(nome_projeto)
        
        elif codigo_tipo == "positivas":
            pasta_originais = Locais().pasta_imagens_positivas_originais(nome_projeto)

        else:
            print(("[ERROR] Tipo de Imagem '" + str(codigo_tipo) + "' indefinido. Processo finalizado."))
            print("         Classe: services.Arquivo.py")
            print("         Método: copiarImagens")
            return

        for imagem in lista_imagens:
            if imagem.lower():
                ex = str(imagem.split(".")[-1])
                if ex == "jpg" or ex == "bmp":
                    img = Image.open(imagem)
                    img.save(os.path.join(pasta_originais, imagem.split("\\")[-1]))

    @staticmethod
    def arquivo_lista(nome_projeto, filtros_imagens):
        """Lista os arquivos de um determinado projeto

            :param nome_projeto: define o nome do projeto
            :type nome_projeto: str

            :param filtros_imagens: recebe uma lista contendo o(s) filtro(s) desejado para aplicação nas imagens
            :type filtros_imagens: str
        """
        
        for i in range(len(filtros_imagens)):
            # Para imagens Cinza
            if filtros_imagens[i] == 1:
                # Positivas
                arquivo = open(os.path.join(Locais().pasta_imagens_positivas_editadas(nome_projeto), "positivas-cinza.txt"), "w+")
                lista_positivas_cinza = Listar().listar_imagens(Locais().pasta_imagens_positivas_editadas_cinza(nome_projeto))
                
                for j in range(len(lista_positivas_cinza)):
                    altura, largura = Imagem().tamanho_imagem(lista_positivas_cinza[j])
                    nome_arquivo = lista_positivas_cinza[j].split("\\")[-1] + " 1 0 0 " + str(largura) + " " + str(altura)
                    linha = str("cinza/" + nome_arquivo)
                    arquivo.write(str(linha))
                    arquivo.write("\n")
                
                # Negativas
                arquivo = open(os.path.join(Locais().pasta_imagens_negativas_editadas(nome_projeto), "negativas-cinza.txt"), "w+")
                lista_negativas_cinza = Listar().listar_imagens(Locais().pasta_imagens_negativas_editadas_cinza(nome_projeto))
                
                for j in range(len(lista_negativas_cinza)):
                    nome_arquivo = lista_negativas_cinza[j].split("\\")[-1]
                    s = os.path.dirname(__file__)
                    base_path = ""
                    
                    for k in range(len(s.split("\\"))):
                        if s.split("\\")[k] == "data":
                            break
                        elif k == 0:
                            base_path = s.split("\\")[k]
                        else:
                            base_path = base_path + "\\" + s.split("\\")[k]
                    
                    linha = os.path.join(base_path, "sample", "projetos", nome_projeto, "negativas", "editadas", "cinza", nome_arquivo)
                    arquivo.write(linha)
                    arquivo.write("\n")
            
            # Para imagens Histograma
            elif filtros_imagens[i] == 2:
                # Positivas
                arquivo = open(os.path.join(Locais().pasta_imagens_positivas_editadas(nome_projeto), "positivas-histograma.txt"), "w+")
                lista_positivas_histograma = Listar().listar_imagens(Locais().pasta_imagens_positivas_editadas_histograma(nome_projeto))
                
                for j in range(len(lista_positivas_histograma)):
                    altura, largura = Imagem().tamanho_imagem(lista_positivas_histograma[j])
                    nome_arquivo = lista_positivas_histograma[j].split("\\")[-1] + " 1 0 0 " + str(largura) + " " + str(altura)
                    linha = str("histograma/" + nome_arquivo)
                    arquivo.write(str(linha))
                    arquivo.write("\n")
                
                # Negativas
                arquivo = open(os.path.join(Locais().pasta_imagens_negativas_editadas(nome_projeto), "negativas-histograma.txt"), "w+")
                lista_negativas_histograma = Listar().listar_imagens(Locais().pasta_imagens_negativas_editadas_histograma(nome_projeto))
                
                for j in range(len(lista_negativas_histograma)):
                    nome_arquivo = lista_negativas_histograma[j].split("\\")[-1]
                    s = os.path.dirname(__file__)
                    base_path = ""
                    
                    for k in range(len(s.split("\\"))):
                        if s.split("\\")[k] == "data":
                            break
                        elif k == 0:
                            base_path = s.split("\\")[k]
                        else:
                            base_path = base_path + "\\" + s.split("\\")[k]
                    
                    linha = os.path.join(base_path, "sample", "projetos", nome_projeto, "negativas", "editadas", "histograma", nome_arquivo)
                    arquivo.write(linha)
                    arquivo.write("\n")
            
            # Para imagens Clahe
            elif filtros_imagens[i] == 3:
                # Positivas
                arquivo = open(os.path.join(Locais().pasta_imagens_positivas_editadas(nome_projeto), "positivas-clahe.txt"), "w+")
                lista_positivas_clahe = Listar().listar_imagens(Locais().pasta_imagens_positivas_editadas_clahe(nome_projeto))
                
                for j in range(len(lista_positivas_clahe)):
                    altura, largura = Imagem().tamanho_imagem(lista_positivas_clahe[j])
                    nome_arquivo = lista_positivas_clahe[j].split("\\")[-1] + " 1 0 0 " + str(largura) + " " + str(altura)
                    linha = str("clahe/" + nome_arquivo)
                    arquivo.write(str(linha))
                    arquivo.write("\n")
                
                # Negativas
                arquivo = open(os.path.join(Locais().pasta_imagens_negativas_editadas(nome_projeto), "negativas-clahe.txt"), "w+")
                lista_negativas_clahe = Listar().listar_imagens(Locais().pasta_imagens_negativas_editadas_clahe(nome_projeto))
                
                for j in range(len(lista_negativas_clahe)):
                    nome_arquivo = lista_negativas_clahe[j].split("\\")[-1]
                    s = os.path.dirname(__file__)
                    base_path = ""
                    
                    for k in range(len(s.split("\\"))):
                        if s.split("\\")[k] == "data":
                            break
                        elif i == 0:
                            base_path = s.split("\\")[k]
                        else:
                            base_path = base_path + "\\" + s.split("\\")[k]
                    
                    linha = os.path.join(base_path, "sample", "projetos", nome_projeto, "negativas", "editadas", "clahe", nome_arquivo)
                    arquivo.write(linha)
                    arquivo.write("\n")
            
            # Não Definido
            elif filtros_imagens[i] == 4:
                pass

    ##########################################################################################
    # Apagar
    ##########################################################################################
    @staticmethod
    def apagar(nome_projeto):
        """Apaga a pasta de um projeto expecifico recursivamente

           :param str nome_projeto: Nome do Projeto
        """
        
        shutil.rmtree(Locais().pasta_projeto(nome_projeto))

    ##########################################################################################
    # Apagar Imagens Teste
    ##########################################################################################
    @staticmethod
    def apagar_imagens_teste(nome_projeto):
        """Apaga as imagens de teste de um projeto expecifico

            :param nome_projeto: Nome do Projeto
            :type nome_projeto: str
        """
        
        # Apaga as pastas das imagens de teste
        shutil.rmtree(os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", nome_projeto, "teste", "imagens", "detectadas"))
        shutil.rmtree(os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", nome_projeto, "teste", "imagens", "originais"))
        
        # Recria as pastas apagadas anteriormente
        os.makedirs(os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", nome_projeto, "teste", "imagens", "detectadas"))
        os.makedirs(os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", nome_projeto, "teste", "imagens", "originais"))
