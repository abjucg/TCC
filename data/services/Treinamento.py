#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import subprocess


class Treinamento(object):
    """Classe responsável por realizar os processos para a criar o treinamento"""

    ##########################################################################################
    # Construtor
    ##########################################################################################

    def __init__(self, nome_projeto, etapa, dados):
        """ Recebe os dados para verificar a etapa relacionada ao treinamento

            :param nome_projeto: recebe o nome do projeto
            :type nome_projeto: str

            :param etapa: recebe um codigo referente a etapa
                s: etapa de criação do arquivo "sample"
                t: etapa relacionada ao treinamento propriamente dito
            :type etapa: str

            :param dados: vetor com os dados de configuração para o treinamento
            :type dados: list
        """
        if etapa == "s":
            self._criar_sample(dados)

        elif etapa == "t":
            self._treinamento(nome_projeto, dados)

        else:
            self._erro_etapa()

    ##########################################################################################
    # Criar arquivo Sample ou vec
    ##########################################################################################
    @staticmethod
    def _criar_sample(dados):
        """ Cria o arquivo .vec a partir do arquivo .txt das imagens positivas

            :param dados: dados que serão utilizados para realizar o treinamento
            :type dados: list
        """

        # Local onde localiza o arquivo executável da OpenCV (opencv_createsamples.exe)        
        local = os.path.join(os.path.dirname(__file__), "..", "..", "sample", "bin", "opencv_createsamples.exe")

        # Localização do arquivo .txt
        info = os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", dados[0])

        # Onde será salvo o arquivo .vec
        vec = os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", dados[1])

        # Ver imagem por imagem
        if dados[3] == "TRUE":
            cmd = str(local + " -info " + info + " -vec " + vec + " -num " + dados[2] + " -show " + dados[3] + " -w " + dados[4] + " -h " + dados[5])

        else:
            cmd = str(local + " -info " + info + " -vec " + vec + " -num " + dados[2] + " -w " + dados[4] + " -h " + dados[5])

        # Executa o subprocess e salva as informações dentro do arquivo de backup das configurações
        subprocess.call(cmd, shell=True)

    ##########################################################################################
    # Realizar Treinamento
    ##########################################################################################
    @staticmethod
    def _treinamento(nome_projeto, dados):
        """ Método responsável por realizar e enviar os dados para o treinamento

            :param nome_projeto: nome do projeto
            :type nome_projeto: str

            :param dados: dados para a realização do treinamento
            :type dados: list
        """

        local = os.path.join(os.path.dirname(__file__), "..", "..", "sample", "bin", "opencv_traincascade.exe")

        data = os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", dados[0])
        bg = os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", dados[1])
        vec = os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", dados[2])

        if os.path.exists(data):
            os.rmdir(data)

        os.makedirs(data)

        cmd = str(local + " -data " + data + " -vec " + vec + " -bg " + bg + " -w " + dados[3] + " -h " + dados[4] + " -numPos " + dados[5] + " -numNeg " + dados[6] + " -numStages " + dados[7] + " -precalcValBufSize " + dados[8] + " -precalcIdxBufSize " + dados[9] + " -numThreads " + dados[11] + " -acceptanceRatioBreakValue " + dados[12] + " -bt " + dados[13] + " -minHitRate " + dados[14] + " -maxFalseAlarmRate " + dados[15] + " -weightTrimRate " + dados[16] + " -maxWeakCount " + dados[18] + " -featureType " + dados[20] + " -mode " + dados[21])

        subprocess.call(cmd, shell=True)

        lista = str(data.split("\\")[-4] + "/" + data.split("\\")[-3] + "/" + data.split("\\")[-2] + "/" + data.split("\\")[-1] + "\n")

        arquivo = open(os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", nome_projeto, "cascade", "listaCascade.txt"), "r")
        conteudo = arquivo.readlines()
        conteudo.append(lista)

        arquivo = open(os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos", nome_projeto, "cascade", "listaCascade.txt"), "w")
        arquivo.writelines(conteudo)
        arquivo.close()

    @staticmethod
    def _erro_etapa():
        print("Codigo etapa inválido")
        return False
