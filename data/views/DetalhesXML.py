#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.views.Testar import Testar
from data.services.LerXML import ler_xml


class DetalhesXML (wx.Frame):
    
    def __init__(self, nome_projeto, cascade):
        """ Construtor da Classe
        :param nome_projeto: Define o nome do projeto utilizado
        :type nome_projeto: str

        :param cascade: Define o diretorio em que se encontra o arquivo cascade do projeto
        :type cascade: str
        """
        
        self.nomeProjeto = nome_projeto
        self.cascade = cascade
        self.detalhes = ""
        
        h = 350
        
        ##########################################################################################
        # Cria o Frame
        ##########################################################################################
        
        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Detalhes | " + nome_projeto + ": " + cascade, pos=wx.DefaultPosition, size=wx.Size(350, h), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer20 = wx.BoxSizer(wx.VERTICAL)

        self.panel4 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer21 = wx.BoxSizer(wx.VERTICAL)

        b_sizer22 = wx.BoxSizer(wx.VERTICAL)

        self.staticText13 = wx.StaticText(self.panel4, wx.ID_ANY, "O arquivo selecionado possui as seguintes configurações: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText13.Wrap(-1)
        b_sizer22.Add(self.staticText13, 0, wx.ALIGN_CENTER | wx.ALL, 0)

        b_sizer21.Add(b_sizer22, 0, wx.ALL | wx.EXPAND, 10)

        b_sizer23 = wx.BoxSizer(wx.VERTICAL)

        list_box3_choices = []
        self.listBox3 = wx.ListBox(self.panel4, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, (h - 140)), list_box3_choices, 0)
        b_sizer23.Add(self.listBox3, 0, wx.EXPAND, 0)

        b_sizer21.Add(b_sizer23, 0, wx.ALL | wx.EXPAND, 10)

        b_sizer24 = wx.BoxSizer(wx.HORIZONTAL)
        
        b_sizer25 = wx.BoxSizer(wx.VERTICAL)
        
        self.buttonOk = wx.Button(self.panel4, wx.ID_ANY, "Voltar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer25.Add(self.buttonOk, 0, wx.ALIGN_CENTER, 0)
        
        b_sizer24.Add(b_sizer25, 1, 0, 0)
        
        b_sizer26 = wx.BoxSizer(wx.VERTICAL)
        
        self.buttonTreinar = wx.Button(self.panel4, wx.ID_ANY, "Testar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer26.Add(self.buttonTreinar, 0, wx.ALIGN_CENTER, 0)
        
        b_sizer24.Add(b_sizer26, 1, 0, 0)
        
        b_sizer21.Add(b_sizer24, 0, wx.ALL | wx.EXPAND, 5)

        self.panel4.SetSizer(b_sizer21)
        b_sizer20.Add(self.panel4, 1, wx.EXPAND, 0)

        self.Centre(wx.BOTH)
        
        self.carregar()
        
        for i in range(len(self.detalhes)):
            self.listBox3.Append(self.detalhes[i][0] + ": " + self.detalhes[i][1])
        
        ##########################################################################################
        # Cursor
        ##########################################################################################
        
        self.buttonOk.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonOk.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        
        self.buttonTreinar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonTreinar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de disparo
        ##########################################################################################
        
        self.buttonOk.Bind(wx.EVT_BUTTON, self._go_main)
        self.buttonTreinar.Bind(wx.EVT_BUTTON, self._go_testar)

    def carregar(self):
        self.detalhes = ler_xml(self.nomeProjeto, self.cascade)

##########################################################################################
# Cursor
##########################################################################################

    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

##########################################################################################
# Fluxo de Tela
##########################################################################################

    def _go_main(self, event):
        from data.views.Main import Main
        Main().Show(True)
        self.Close(True)

    def _go_testar(self, event):
        Testar(self.nomeProjeto, self.cascade).Show(True)
        self.Close(True)
