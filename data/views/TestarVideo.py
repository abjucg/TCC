#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import wx
import wx.media


class TestarVideo(wx.Frame):

    def __init__(self, nome_projeto, arquivo_cascade, nome_video):
        self._nomeProjeto = nome_projeto
        self._arquivoCascade = arquivo_cascade
        w, h = 800, 550

        local_atual = os.path.dirname(__file__)

        aux = ""

        for i in range(len(local_atual) - 10):
            aux = aux + local_atual[i]

        local_video = str(os.path.join(aux, "sample", "projetos", nome_projeto, "teste", "videos", nome_video))

        ##########################################################################################
        # Cria Frame
        ##########################################################################################

        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Testar 2-2: Video | " + nome_projeto, pos=wx.DefaultPosition,
                          size=wx.Size(w, (h + 50)), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer1 = wx.BoxSizer(wx.VERTICAL)

        self.panel1 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer2 = wx.BoxSizer(wx.VERTICAL)

        b_sizer52 = wx.BoxSizer(wx.HORIZONTAL)

        self.mediaCtrlVideo = wx.media.MediaCtrl(self.panel1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                 wx.Size(w, h))
        self.mediaCtrlVideo.Load(local_video)
        self.mediaCtrlVideo.SetPlaybackRate(1)
        self.mediaCtrlVideo.SetVolume(0)
        self.mediaCtrlVideo.ShowPlayerControls(0)

        b_sizer52.Add(self.mediaCtrlVideo, 0, wx.ALL, 5)

        b_sizer2.Add(b_sizer52, 1, wx.EXPAND, 5)

        self.staticline4 = wx.StaticLine(self.panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        b_sizer2.Add(self.staticline4, 0, wx.BOTTOM | wx.EXPAND | wx.TOP, 5)

        b_sizer6 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer8 = wx.BoxSizer(wx.VERTICAL)

        self.buttonNovoTeste = wx.Button(self.panel1, wx.ID_ANY, "Novo Teste", wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer8.Add(self.buttonNovoTeste, 0, wx.ALIGN_LEFT | wx.ALL, 5)

        b_sizer6.Add(b_sizer8, 1, wx.EXPAND, 5)

        b_sizer9 = wx.BoxSizer(wx.VERTICAL)

        self.buttonSair = wx.Button(self.panel1, wx.ID_ANY, "Sair", wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer9.Add(self.buttonSair, 0, wx.ALIGN_RIGHT | wx.ALL, 5)

        b_sizer6.Add(b_sizer9, 1, wx.EXPAND, 5)

        b_sizer2.Add(b_sizer6, 0, wx.ALL | wx.EXPAND, 5)

        self.panel1.SetSizer(b_sizer2)
        self.panel1.Layout()
        b_sizer1.Add(self.panel1, 1, wx.EXPAND, 0)

        self.Centre(wx.BOTH)

        self.Show(True)

        ##########################################################################################
        # Globais
        ##########################################################################################
        self.mediaCtrlVideo.Bind(wx.media.EVT_MEDIA_STOP, self._iniciar_video)

        ##########################################################################################
        # Cursor
        ##########################################################################################
        self.buttonNovoTeste.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonNovoTeste.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonSair.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonSair.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de Disparo
        ##########################################################################################
        # Chama o metodo _go_novo_teste para fechar esta view e abrir a view Testar.py
        self.buttonNovoTeste.Bind(wx.EVT_BUTTON, self._go_novo_teste)

        # Chama o metodo _go_sair para fechar esta view e abrir a view Main.py
        self.buttonSair.Bind(wx.EVT_BUTTON, self._go_sair)

    ##########################################################################################
    # Metodos Globais
    ##########################################################################################
    def _iniciar_video(self, event):
        self.mediaCtrlVideo.Play()

    ##########################################################################################
    # Cursor
    ##########################################################################################
    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

    ##########################################################################################
    # Fluxo de Tela
    ##########################################################################################
    def _go_novo_teste(self, event):
        self.Close(True)
        from data.views.Testar import Testar
        Testar(self._nomeProjeto, self._arquivoCascade).Show(True)

    def _go_sair(self, event):
        self.Close(True)
        from data.views.Main import Main
        Main().Show(True)
