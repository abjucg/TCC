#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.views.RealizarTreinamento import RealizarTreinamento


class TamanhoMaximo(wx.Frame):

	def __init__(self, projeto):

		self._projeto = projeto

		##########################################################################################
		# Cria o Frame
		##########################################################################################

		wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Novo Projeto 4-6: Tamanho Máximo | " + projeto.get_nome_projeto(), pos=wx.DefaultPosition, size=wx.Size(350, 184), style=wx.CAPTION | wx.TAB_TRAVERSAL)

		self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

		b_sizer_frame = wx.BoxSizer(wx.VERTICAL)
		
		self.panelMain = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
		b_sizer486 = wx.BoxSizer(wx.VERTICAL)
		
		b_sizer112 = wx.BoxSizer(wx.VERTICAL)
		
		self.staticText33 = wx.StaticText(self.panelMain, wx.ID_ANY, "Informe o tamanho desejado para as imagens Positivas", wx.DefaultPosition, wx.DefaultSize, 0)
		self.staticText33.Wrap(-1)
		b_sizer112.Add(self.staticText33, 0, wx.ALIGN_CENTER | wx.ALL, 0)
		
		b_sizer120 = wx.BoxSizer(wx.HORIZONTAL)
		
		self.staticText333 = wx.StaticText(self.panelMain, wx.ID_ANY, "(As imagens negativas são ", wx.DefaultPosition, wx.DefaultSize, 0)
		self.staticText333.Wrap(-1)
		b_sizer120.Add(self.staticText333, 0, 0, 0)
		
		self.staticText37 = wx.StaticText(self.panelMain, wx.ID_ANY, "2x", wx.DefaultPosition, wx.DefaultSize, 0)
		self.staticText37.Wrap(-1)
		self.staticText37.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))
		
		b_sizer120.Add(self.staticText37, 0, 0, 0)
		
		self.staticText38 = wx.StaticText(self.panelMain, wx.ID_ANY, " este tamanho)", wx.DefaultPosition, wx.DefaultSize, 0)
		self.staticText38.Wrap(-1)
		b_sizer120.Add(self.staticText38, 0, 0, 0)
		
		b_sizer112.Add(b_sizer120, 0, wx.ALIGN_CENTER | wx.ALL, 0)
		
		b_sizer486.Add(b_sizer112, 0, wx.ALL | wx.EXPAND, 10)
		
		b_sizer482 = wx.BoxSizer(wx.VERTICAL)
		
		tamanho1 = wx.BoxSizer(wx.HORIZONTAL)
		
		self.staticText295 = wx.StaticText(self.panelMain, wx.ID_ANY, "Tamanho: ", wx.DefaultPosition, wx.DefaultSize, 0)
		self.staticText295.Wrap(-1)
		tamanho1.Add(self.staticText295, 0, wx.BOTTOM | wx.RIGHT | wx.TOP, 3)
		
		self.textCtrlTamanhoMaximo = wx.TextCtrl(self.panelMain, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(75, -1), 0)
		tamanho1.Add(self.textCtrlTamanhoMaximo, 1, 0, 0)
		
		self.staticText39 = wx.StaticText(self.panelMain, wx.ID_ANY, " px", wx.DefaultPosition, wx.DefaultSize, 0)
		self.staticText39.Wrap(-1)
		tamanho1.Add(self.staticText39, 0, wx.ALL, 3)
		
		b_sizer482.Add(tamanho1, 0, 0, 0)
		
		b_sizer486.Add(b_sizer482, 0, wx.ALIGN_CENTER | wx.ALL, 10)
		
		b_sizer484 = wx.BoxSizer(wx.HORIZONTAL)
		
		b_sizer487 = wx.BoxSizer(wx.HORIZONTAL)
		
		b_sizer43 = wx.BoxSizer(wx.VERTICAL)
		
		self.buttonCancelar = wx.Button(self.panelMain, wx.ID_ANY, "Cancelar", wx.DefaultPosition, wx.Size(130, 30), 0)
		b_sizer43.Add(self.buttonCancelar, 0, wx.ALIGN_LEFT | wx.RIGHT, 0)
		
		b_sizer487.Add(b_sizer43, 1, wx.EXPAND, 5)
		
		b_sizer44 = wx.BoxSizer(wx.VERTICAL)
		
		self.buttonAvancar = wx.Button(self.panelMain, wx.ID_ANY, "Avançar >", wx.DefaultPosition, wx.Size(130, 30), 0)
		b_sizer44.Add(self.buttonAvancar, 0, wx.ALIGN_CENTER | wx.ALIGN_RIGHT, 0)
		
		b_sizer487.Add(b_sizer44, 1, wx.EXPAND, 5)
		
		b_sizer484.Add(b_sizer487, 1, 0, 5)
		
		b_sizer486.Add(b_sizer484, 0, wx.ALL | wx.EXPAND, 10)
		
		self.panelMain.SetSizer(b_sizer486)
		self.panelMain.Layout()
		b_sizer_frame.Add(self.panelMain, 0, wx.EXPAND, 0)
				
		self.Centre(wx.BOTH)

		##########################################################################################
		# Globais
		##########################################################################################
		
		self.textCtrlTamanhoMaximo.Bind(wx.EVT_TEXT, self._habilitar_avancar)

		##########################################################################################
		# Configura Tela
		##########################################################################################
		
		# Desabilita o botão Avançar
		self.buttonAvancar.Enable(False)

		##########################################################################################
		# Cursor
		##########################################################################################

		self.buttonCancelar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
		self.buttonCancelar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
		
		self.buttonAvancar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
		self.buttonAvancar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

		##########################################################################################
		# Eventos de disparo
		##########################################################################################
		
		# Chama o metodo _goRealizarTreinameno para fechar a view atual e abrir a view RealizarTreinamento.py
		self.buttonAvancar.Bind(wx.EVT_BUTTON, self._go_realizar_treinamento)
		
		# Chama o metodo _fechar para fechar a view atual e cancelar o processo
		self.buttonCancelar.Bind(wx.EVT_BUTTON, self._fechar)

##########################################################################################
# Metodos Globais
##########################################################################################

	def _habilitar_avancar(self, event):
		self.buttonAvancar.Enable(True)

##########################################################################################
# Cursor
##########################################################################################

	def _mudar_cursor(self, event):
		self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
		self.Refresh()

	def _cursor_normal(self, event):
		self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
		self.Refresh()

##########################################################################################
# Fluxo de Tela
##########################################################################################

	def _go_realizar_treinamento(self, event):
		self._projeto.set_tamanho_maximo(int(self.textCtrlTamanhoMaximo.GetValue()))
		RealizarTreinamento(self._projeto).Show(True)
		self.Close(True)
	
	def _fechar(self, event):
		self.Close(True)
		
		from data.views.Main import Main
		Main().Show(True)
