#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.services.Resultado import Resultado


class TestarImagensResultado(wx.Frame):
    """Classe responsável por exibir os resultados do teste em imagens"""

    def __init__(self, nome_projeto, resultados, arquivo_cascade):
        """Controi a tela responsável por exibir o resultado da detecção
            :param nome_projeto: define o nome do projeto utilizado
            :type nome_projeto: str

            :param resultados: recebe uma matriz contendo os dados de todas as detecções
            :type resultados: list(list(detecções, positivo, falso-positivo, falso-negativo))

            :param arquivo_cascade: nome do arquivo cascade utilizado
            :type arquivo_cascade: str
        """

        self._nomeProjeto = nome_projeto
        self._arquivoCascade = arquivo_cascade

        # Calculando o rendimento das detecções realizadas
        acerto, erro = Resultado().calcular(resultados)

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################

        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Testar 3-3: Imagens - Resultado | " + nome_projeto, pos=wx.DefaultPosition, size=wx.Size(500, 250), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer1 = wx.BoxSizer(wx.VERTICAL)

        self.panel1 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer2 = wx.BoxSizer(wx.VERTICAL)

        sb_sizer5 = wx.StaticBoxSizer(wx.StaticBox(self.panel1, wx.ID_ANY, "Resultados da Detecção"), wx.VERTICAL)

        b_sizer109 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer105 = wx.BoxSizer(wx.VERTICAL)

        self.staticText573 = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "Arquivo Cascade: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText573.Wrap(-1)
        b_sizer105.Add(self.staticText573, 0, wx.ALL, 5)

        self.staticText57 = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "Total da Amostra: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText57.Wrap(-1)
        b_sizer105.Add(self.staticText57, 0, wx.ALL, 5)

        self.staticText59 = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "Acerto: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText59.Wrap(-1)
        b_sizer105.Add(self.staticText59, 0, wx.ALL, 5)

        self.staticText60 = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "Erro: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText60.Wrap(-1)
        b_sizer105.Add(self.staticText60, 0, wx.ALL, 5)

        b_sizer109.Add(b_sizer105, 1, wx.ALL, 5)

        b_sizer106 = wx.BoxSizer(wx.VERTICAL)

        self.staticTextArquivoCascade = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextArquivoCascade.Wrap(-1)
        self.staticTextArquivoCascade.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer106.Add(self.staticTextArquivoCascade, 0, wx.ALL, 5)

        self.staticTextTotalAmostra = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextTotalAmostra.Wrap(-1)
        self.staticTextTotalAmostra.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer106.Add(self.staticTextTotalAmostra, 0, wx.ALL, 5)

        self.staticTextAcerto = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextAcerto.Wrap(-1)
        self.staticTextAcerto.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer106.Add(self.staticTextAcerto, 0, wx.ALL, 5)

        self.staticTextErro = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextErro.Wrap(-1)
        self.staticTextErro.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer106.Add(self.staticTextErro, 0, wx.ALL, 5)

        b_sizer109.Add(b_sizer106, 0, wx.ALL, 5)

        sb_sizer5.Add(b_sizer109, 0, wx.ALIGN_CENTER, 0)

        b_sizer2.Add(sb_sizer5, 0, wx.ALL | wx.EXPAND, 10)

        b_sizer6 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer108 = wx.BoxSizer(wx.VERTICAL)

        self.buttonNovaDeteccao = wx.Button(self.panel1, wx.ID_ANY, "Nova Detecção", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer108.Add(self.buttonNovaDeteccao, 0, 0, 0)

        b_sizer6.Add(b_sizer108, 1, wx.EXPAND, 0)

        b_sizer8 = wx.BoxSizer(wx.VERTICAL)

        self.buttonRelatorio = wx.Button(self.panel1, wx.ID_ANY, "Relatório", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer8.Add(self.buttonRelatorio, 0, wx.ALIGN_CENTER, 0)

        b_sizer6.Add(b_sizer8, 1, wx.EXPAND, 0)

        b_sizer9 = wx.BoxSizer(wx.VERTICAL)

        self.buttonSair = wx.Button(self.panel1, wx.ID_ANY, "Sair", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer9.Add(self.buttonSair, 0, wx.ALIGN_RIGHT, 0)

        b_sizer6.Add(b_sizer9, 1, wx.EXPAND, 0)

        b_sizer2.Add(b_sizer6, 0, wx.ALL | wx.EXPAND, 10)

        self.panel1.SetSizer(b_sizer2)
        self.panel1.Layout()
        b_sizer1.Add(self.panel1, 1, wx.EXPAND, 0)

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Globais
        ##########################################################################################

        self.staticTextArquivoCascade.SetLabel(arquivo_cascade)
        self.staticTextTotalAmostra.SetLabel(str(len(resultados)))
        self.staticTextAcerto.SetLabel(str(acerto) + "%")
        self.staticTextErro.SetLabel(str(erro) + "%")

        self.buttonRelatorio.Hide()

        ##########################################################################################
        # Cursor
        ##########################################################################################

        self.buttonNovaDeteccao.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonNovaDeteccao.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonRelatorio.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonRelatorio.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonSair.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonSair.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de desparo do usuário
        ##########################################################################################

        # Chama o método _go_nova_deteccao para fechar esta janela e ir para a janela inicial dos testes
        self.buttonNovaDeteccao.Bind(wx.EVT_BUTTON, self._go_nova_deteccao)

        # Chama o método _go_relatorio para exibir o relatorio da detecção realizada
        self.buttonRelatorio.Bind(wx.EVT_BUTTON, self._go_relatorio)

        # Chama o método _goPrincipal() para fechar esta janela e ir para a janela principal do sistema
        self.buttonSair.Bind(wx.EVT_BUTTON, self._sair)

    ##########################################################################################
    # Cursor
    ##########################################################################################
    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

    ##########################################################################################
    # Métodos de Transição de Tela
    ##########################################################################################
    def _sair(self, event):
        self.Close(True)
        from data.views.Main import Main
        Main().Show(True)

    def _go_nova_deteccao(self, event):
        self.Close(True)
        from data.views.Testar import Testar
        Testar(self._nomeProjeto, self._arquivoCascade).Show(True)

    def _go_relatorio(self, event):
        pass
