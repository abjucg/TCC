#!/usr/bin/env python
# -*- coding: utf-8 -*-  

import wx
from data.views.TamanhoMaximo import TamanhoMaximo


class FiltroImagens (wx.Frame):

    def __init__(self, projeto):
        """ Construtor da Classe
            :param projeto: Define o nome do projeto
            :type projeto: str
        """

        self._projeto = projeto

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################
        
        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Novo Projeto 3-6: Selecionar Filtros | " + projeto.get_nome_projeto(), pos=wx.DefaultPosition, size=wx.Size(350, 185), style=wx.CAPTION | wx.TAB_TRAVERSAL)
        
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        
        b_sizer113 = wx.BoxSizer(wx.VERTICAL)
        
        self.panel10 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer114 = wx.BoxSizer(wx.VERTICAL)
        
        b_sizer484 = wx.BoxSizer(wx.VERTICAL)
        
        b_sizer109 = wx.BoxSizer(wx.VERTICAL)
        
        self.staticText23 = wx.StaticText(self.panel10, wx.ID_ANY, "Deseja aplicar algum filtro nas imagem?", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText23.Wrap(-1)
        b_sizer109.Add(self.staticText23, 0, wx.ALL | wx.EXPAND, 5)
        
        b_sizer484.Add(b_sizer109, 0, wx.ALIGN_CENTER | wx.LEFT | wx.RIGHT | wx.TOP, 10)
        
        b_sizer482 = wx.BoxSizer(wx.VERTICAL)
        
        self.checkBoxCinza = wx.CheckBox(self.panel10, wx.ID_ANY, "Cinza", wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer482.Add(self.checkBoxCinza, 0, wx.ALL, 5)
        
        self.checkBoxHistograma = wx.CheckBox(self.panel10, wx.ID_ANY, "Equalização do Histograma", wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer482.Add(self.checkBoxHistograma, 0, wx.ALL, 5)
        
        # self.checkBoxClahe = wx.CheckBox(self.panel10, wx.ID_ANY, "CLAHE", wx.DefaultPosition, wx.DefaultSize, 0)
        # b_sizer482.Add(self.checkBoxClahe, 0, wx.ALL, 5)
        
        b_sizer484.Add(b_sizer482, 0, wx.ALL | wx.EXPAND, 5)
        
        b_sizer487 = wx.BoxSizer(wx.HORIZONTAL)
        
        b_sizer43 = wx.BoxSizer(wx.VERTICAL)
        
        self.buttonCancelar = wx.Button(self.panel10, wx.ID_ANY, "Cancelar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer43.Add(self.buttonCancelar, 0, wx.ALIGN_LEFT | wx.RIGHT, 0)
        
        b_sizer487.Add(b_sizer43, 1, 0, 0)
        
        b_sizer44 = wx.BoxSizer(wx.VERTICAL)
        
        self.buttonAvancar = wx.Button(self.panel10, wx.ID_ANY, "Avançar >", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer44.Add(self.buttonAvancar, 0, wx.ALIGN_CENTER | wx.ALIGN_RIGHT, 0)
        
        b_sizer487.Add(b_sizer44, 1, 0, 0)
        
        b_sizer484.Add(b_sizer487, 0, wx.ALL | wx.EXPAND, 10)
        
        b_sizer114.Add(b_sizer484, 1, wx.EXPAND, 5)
        
        self.panel10.SetSizer(b_sizer114)
        self.panel10.Layout()
        b_sizer113.Add(self.panel10, 1, wx.EXPAND, 0)
        
        self.SetSizer(b_sizer113)
        self.Layout()
        
        self.Centre(wx.BOTH)
        
        ##########################################################################################
        # Configura Tela
        ##########################################################################################
        
        # Marca o checkBoxCinza
        self.checkBoxCinza.SetValue(True)
        
        # Desabilita o checkBoxCinza
        self.checkBoxCinza.Enable(False)
        
        ##########################################################################################
        # Cursor
        ##########################################################################################
        
        self.buttonCancelar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonCancelar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        
        self.buttonAvancar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonAvancar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de disparo
        ##########################################################################################
        
        # Chama o metodo _goTamanhoMaximo para fechar esta view e abrir a RealizarTreinamento.py
        self.buttonAvancar.Bind(wx.EVT_BUTTON, self._go_tamanho_maximo)
        
        # Chama o metodo _fechar para fechar esta view
        self.buttonCancelar.Bind(wx.EVT_BUTTON, self._fechar)

##########################################################################################
# Cursor
##########################################################################################

    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

##########################################################################################
# Fluxo de Tela
##########################################################################################
  
    def _go_tamanho_maximo(self, event):
        self._projeto.set_filtros_imagens(1)
            
        if self.checkBoxHistograma.IsChecked():
            self._projeto.set_filtros_imagens(2)
            
        # if self.checkBoxClahe.IsChecked():
        #     self._projeto.set_filtros_imagens(3)
        
        TamanhoMaximo(self._projeto).Show(True)
        self.Close(True)

    def _fechar(self, event):
        self.Close(True)
        from data.views.Main import Main
        Main().Show(True)
