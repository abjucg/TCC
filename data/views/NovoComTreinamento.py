#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.views.TreinarTipoImagem import TreinarTipoImagem


class NovoComTreino(wx.Frame):

    def __init__(self, projeto):
        """ Construtor da Classe
            :param projeto: Define o conjunto de caracteristicas de um projeto
            :type projeto: list
        """

        self._projeto = projeto

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################
        
        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Novo Projeto 6-6: Finalizado | " + projeto.get_nome_projeto(), pos=wx.DefaultPosition, size=wx.Size(350, 140), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer_frame = wx.BoxSizer(wx.VERTICAL)

        self.panelMain = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer486 = wx.BoxSizer(wx.VERTICAL)

        b_sizer482 = wx.BoxSizer(wx.HORIZONTAL)

        self.staticText22 = wx.StaticText(self.panelMain, wx.ID_ANY, "Projeto ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText22.Wrap(-1)
        b_sizer482.Add(self.staticText22, 0, wx.ALIGN_CENTER, 0)

        self.staticTextNomeProjeto = wx.StaticText(self.panelMain, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNomeProjeto.Wrap(-1)
        self.staticTextNomeProjeto.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer482.Add(self.staticTextNomeProjeto, 0, wx.ALIGN_CENTER, 0)

        self.staticText28 = wx.StaticText(self.panelMain, wx.ID_ANY, " criado com sucesso!", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText28.Wrap(-1)
        b_sizer482.Add(self.staticText28, 0, wx.ALIGN_CENTER, 0)

        b_sizer486.Add(b_sizer482, 0, wx.ALIGN_CENTER | wx.LEFT | wx.RIGHT | wx.TOP, 10)

        b_sizer60 = wx.BoxSizer(wx.VERTICAL)

        self.staticText15 = wx.StaticText(self.panelMain, wx.ID_ANY, "Iniciando o Processo de Treinamento", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText15.Wrap(-1)
        b_sizer60.Add(self.staticText15, 0, wx.ALIGN_CENTER, 0)

        b_sizer486.Add(b_sizer60, 1, wx.ALIGN_CENTER | wx.BOTTOM, 10)

        b_sizer484 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer44 = wx.BoxSizer(wx.VERTICAL)

        self.buttonOk = wx.Button(self.panelMain, wx.ID_ANY, "Ok", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer484.Add(self.buttonOk, 0, 0, 0)

        b_sizer486.Add(b_sizer484, 0, wx.ALIGN_CENTER | wx.ALL, 10)

        self.panelMain.SetSizer(b_sizer486)
        self.panelMain.Layout()
        b_sizer_frame.Add(self.panelMain, 1, wx.EXPAND, 0)

        self.Centre(wx.BOTH)
        
        ##########################################################################################
        # Ações Globais
        ##########################################################################################
        
        # Aplica o nome do projeto no Label staticTextNomeProjeto
        self.staticTextNomeProjeto.SetLabel(self._projeto.get_nome_projeto())

        ##########################################################################################
        # Cursor
        ##########################################################################################
        
        self.buttonOk.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonOk.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de disparo
        ##########################################################################################
        
        # Chama o metodo _treinar para fechar esta view, finalizar o processo de criar um projeto e iniciar o treinamenot
        self.buttonOk.Bind(wx.EVT_BUTTON, self._treinar)

##########################################################################################
# Cursor
##########################################################################################

    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

##########################################################################################
# Fluxo de Tela
##########################################################################################

    def _treinar(self, event):
        TreinarTipoImagem(self._projeto.get_nome_projeto()).Show(True)
        self.Close(True)
