#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.services.Projeto import Projeto


class ResultadoApagar(wx.Frame):

    def __init__(self, nome_projeto):

        self._nomeProjeto = nome_projeto

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################
        
        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Atenção", pos=wx.DefaultPosition, size=wx.Size(350, 135), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer19 = wx.BoxSizer(wx.VERTICAL)

        self.panel5 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer20 = wx.BoxSizer(wx.VERTICAL)

        b_sizer21 = wx.BoxSizer(wx.HORIZONTAL)

        self.staticText10 = wx.StaticText(self.panel5, wx.ID_ANY, "O Projeto ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText10.Wrap(-1)
        b_sizer21.Add(self.staticText10, 0, 0, 0)

        self.staticTextNomeProjeto = wx.StaticText(self.panel5, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNomeProjeto.Wrap(-1)
        self.staticTextNomeProjeto.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, True, wx.EmptyString))

        b_sizer21.Add(self.staticTextNomeProjeto, 0, 0, 0)

        self.staticTextResultado = wx.StaticText(self.panel5, wx.ID_ANY, " foi apagado com sucesso!", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextResultado.Wrap(-1)
        self.staticTextResultado.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer21.Add(self.staticTextResultado, 0, 0, 0)

        b_sizer20.Add(b_sizer21, 0, wx.ALIGN_CENTER | wx.LEFT | wx.RIGHT | wx.TOP, 10)

        b_sizer23 = wx.BoxSizer(wx.HORIZONTAL)

        self.staticText14 = wx.StaticText(self.panel5, wx.ID_ANY, "Clique no botão ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText14.Wrap(-1)
        b_sizer23.Add(self.staticText14, 0, 0, 0)

        self.staticText15 = wx.StaticText(self.panel5, wx.ID_ANY, "OK", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText15.Wrap(-1)
        self.staticText15.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer23.Add(self.staticText15, 0, 0, 0)

        self.staticText16 = wx.StaticText(self.panel5, wx.ID_ANY, " para retornar a página inicial.", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText16.Wrap(-1)
        b_sizer23.Add(self.staticText16, 0, 0, 0)

        b_sizer20.Add(b_sizer23, 0, wx.ALIGN_CENTER | wx.ALL, 5)

        b_sizer22 = wx.BoxSizer(wx.VERTICAL)

        self.buttonOK = wx.Button(self.panel5, wx.ID_ANY, "OK", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer22.Add(self.buttonOK, 0, 0, 0)

        b_sizer20.Add(b_sizer22, 0, wx.ALIGN_CENTER | wx.ALL, 5)

        self.panel5.SetSizer(b_sizer20)
        self.panel5.Layout()
        b_sizer19.Add(self.panel5, 1, wx.EXPAND, 0)

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Globais
        ##########################################################################################
        
        self.staticTextNomeProjeto.SetLabel(nome_projeto)
        
        ##########################################################################################
        # Cursor
        ##########################################################################################
        
        self.buttonOK.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonOK.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de disparo
        ##########################################################################################
        
        # Chama o metodo _goMain para apagar o projeto, abrir a view Main.py e fechar esta view
        self.buttonOK.Bind(wx.EVT_BUTTON, self._go_main)

##########################################################################################
# Cursor
##########################################################################################

    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

##########################################################################################
# Fluxo de Tela
##########################################################################################

    def _go_main(self, event):
        Projeto().apagar_projeto(self._nomeProjeto)

        from data.views.Main import Main
        Main().Show(True)
        self.Close(True)
