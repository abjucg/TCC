#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
import os.path
from data.services.Imagem import Imagem
from data.views.TreinarCascade import TreinarCascade


class TreinarSample (wx.Frame):

    def __init__(self, nome_projeto, filtro, w="", h=""):

        self.nomeProjeto = nome_projeto
        self.filtro = filtro
        self.w = w
        self.h = h

        ##########################################################################################
        # Cria Frame
        ##########################################################################################

        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Treinamento 2-3: Sample | " + nome_projeto, pos=wx.DefaultPosition, size=wx.Size(450, 350), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer40 = wx.BoxSizer(wx.VERTICAL)

        self.panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer41 = wx.BoxSizer(wx.VERTICAL)

        sb_sizer5 = wx.StaticBoxSizer(wx.StaticBox(self.panel, wx.ID_ANY, "Informações Básicas"), wx.HORIZONTAL)

        b_sizer47 = wx.BoxSizer(wx.VERTICAL)

        self.staticText32 = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "vec:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText32.Wrap(-1)
        b_sizer47.Add(self.staticText32, 0, wx.ALL, 5)

        self.staticText35 = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "info:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText35.Wrap(-1)
        b_sizer47.Add(self.staticText35, 0, wx.ALL, 5)

        self.staticText33 = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "num:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText33.Wrap(-1)
        b_sizer47.Add(self.staticText33, 0, wx.ALL, 5)

        sb_sizer5.Add(b_sizer47, 0, wx.EXPAND, 5)

        b_sizer48 = wx.BoxSizer(wx.VERTICAL)

        self.staticTextVec = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextVec.Wrap(-1)
        self.staticTextVec.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer48.Add(self.staticTextVec, 0, wx.ALL, 5)

        self.staticTextInfo = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextInfo.Wrap(-1)
        self.staticTextInfo.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer48.Add(self.staticTextInfo, 0, wx.ALL, 5)

        self.staticTextNum = wx.StaticText(sb_sizer5.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNum.Wrap(-1)
        self.staticTextNum.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer48.Add(self.staticTextNum, 0, wx.ALL, 5)

        sb_sizer5.Add(b_sizer48, 0, wx.EXPAND, 5)

        b_sizer41.Add(sb_sizer5, 0, wx.ALL | wx.EXPAND, 5)

        sb_sizer_multiplas = wx.StaticBoxSizer(wx.StaticBox(self.panel, wx.ID_ANY, "Dados para Criação | Tipo Múltiplas Imagens"), wx.VERTICAL)

        b_sizer55 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77 = wx.StaticText(sb_sizer_multiplas.GetStaticBox(), wx.ID_ANY, "show:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText77.Wrap(-1)
        b_sizer57.Add(self.staticText77, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55.Add(b_sizer57, 1, wx.EXPAND, 0)

        b_sizer58 = wx.BoxSizer(wx.VERTICAL)

        choice_show_choices = ["TRUE", "FALSE"]
        self.choiceShow = wx.Choice(sb_sizer_multiplas.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size(110, -1), choice_show_choices, 0)
        self.choiceShow.SetSelection(1)
        b_sizer58.Add(self.choiceShow, 0, wx.ALL, 5)

        b_sizer55.Add(b_sizer58, 0, wx.EXPAND, 0)

        b_sizer82 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84 = wx.StaticText(sb_sizer_multiplas.GetStaticBox(), wx.ID_ANY, "default: FALSE", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText84.Wrap(-1)
        b_sizer82.Add(self.staticText84, 0, wx.ALL, 8)

        b_sizer55.Add(b_sizer82, 1, wx.EXPAND, 5)

        sb_sizer_multiplas.Add(b_sizer55, 0, wx.EXPAND, 5)

        b_sizer551 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer571 = wx.BoxSizer(wx.VERTICAL)

        self.staticText771 = wx.StaticText(sb_sizer_multiplas.GetStaticBox(), wx.ID_ANY, "w:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText771.Wrap(-1)
        b_sizer571.Add(self.staticText771, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer551.Add(b_sizer571, 1, wx.EXPAND, 0)

        b_sizer581 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlW = wx.TextCtrl(sb_sizer_multiplas.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer581.Add(self.textCtrlW, 0, wx.ALL, 5)

        b_sizer551.Add(b_sizer581, 0, wx.EXPAND, 0)

        b_sizer821 = wx.BoxSizer(wx.VERTICAL)

        self.staticText841 = wx.StaticText(sb_sizer_multiplas.GetStaticBox(), wx.ID_ANY, "default: 24", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText841.Wrap(-1)
        b_sizer821.Add(self.staticText841, 0, wx.ALL, 8)

        b_sizer551.Add(b_sizer821, 1, wx.EXPAND, 5)

        sb_sizer_multiplas.Add(b_sizer551, 0, wx.EXPAND, 5)

        b_sizer552 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer572 = wx.BoxSizer(wx.VERTICAL)

        self.staticText772 = wx.StaticText(sb_sizer_multiplas.GetStaticBox(), wx.ID_ANY, "h:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText772.Wrap(-1)
        b_sizer572.Add(self.staticText772, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer552.Add(b_sizer572, 1, wx.EXPAND, 0)

        b_sizer582 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlH = wx.TextCtrl(sb_sizer_multiplas.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer582.Add(self.textCtrlH, 0, wx.ALL, 5)

        b_sizer552.Add(b_sizer582, 0, wx.EXPAND, 0)

        b_sizer822 = wx.BoxSizer(wx.VERTICAL)

        self.staticText842 = wx.StaticText(sb_sizer_multiplas.GetStaticBox(), wx.ID_ANY, "default: 24", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText842.Wrap(-1)
        b_sizer822.Add(self.staticText842, 0, wx.ALL, 8)

        b_sizer552.Add(b_sizer822, 1, wx.EXPAND, 5)

        sb_sizer_multiplas.Add(b_sizer552, 0, wx.EXPAND, 5)

        b_sizer41.Add(sb_sizer_multiplas, 1, wx.ALL | wx.EXPAND, 5)

        b_sizer43 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer45 = wx.BoxSizer(wx.VERTICAL)

        self.buttonCancelar = wx.Button(self.panel, wx.ID_ANY, "Cancelar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer45.Add(self.buttonCancelar, 0, wx.ALIGN_CENTER | wx.ALL, 5)

        b_sizer43.Add(b_sizer45, 1, 0, 0)

        b_sizer46 = wx.BoxSizer(wx.VERTICAL)

        self.buttonAvancar = wx.Button(self.panel, wx.ID_ANY, "Avançar >", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer46.Add(self.buttonAvancar, 0, wx.ALIGN_CENTER | wx.ALL, 5)

        b_sizer43.Add(b_sizer46, 1, 0, 0)

        b_sizer41.Add(b_sizer43, 0, wx.ALL | wx.EXPAND, 5)

        self.panel.SetSizer(b_sizer41)
        self.panel.Layout()
        b_sizer40.Add(self.panel, 0, wx.EXPAND, 0)

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Globais
        ##########################################################################################

        self.staticTextVec.SetLabel(str(os.path.join(nome_projeto, 'positivas', 'editadas', str('positivas-' + filtro + '.vec'))))
        self.staticTextInfo.SetLabel(str(os.path.join(nome_projeto, 'positivas', 'editadas', str('positivas-' + filtro + '.txt'))))
        self.staticTextNum.SetLabel(str(Imagem().contar_imagens(nome_projeto, "p", "editadas\\" + filtro)))

        if w == "":
            self.textCtrlW.SetLabel(str(24))
        else:
            self.textCtrlW.SetLabel(w)

        if h == "":
            self.textCtrlH.SetLabel(str(24))
        else:
            self.textCtrlH.SetLabel(h)

        ##########################################################################################
        # Cursor
        ##########################################################################################
        self.buttonAvancar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonAvancar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonCancelar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonCancelar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de Disparo
        ##########################################################################################
        self.buttonAvancar.Bind(wx.EVT_BUTTON, self._avancar)
        self.buttonCancelar.Bind(wx.EVT_BUTTON, self._cancelar)

    ##########################################################################################
    # Cursor
    ##########################################################################################
    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

    ##########################################################################################
    # Fluxo de Tela
    ##########################################################################################
    def _cancelar(self, event):
        from data.views.Main import Main
        Main().Show(True)
        self.Close(True)

    def _avancar(self, event):
        TreinarCascade(self.nomeProjeto, self.staticTextInfo.GetLabel(), self.staticTextVec.GetLabel(), self.staticTextNum.GetLabel(), self.filtro, self.choiceShow.GetStringSelection(), self.textCtrlW.GetValue(), self.textCtrlH.GetValue()).Show()
        self.Close()


