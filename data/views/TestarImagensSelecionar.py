#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.views.TestarImagens import TestarImagens


class TestarImagensSelecionar (wx.Frame):
    """Classe responsável por criar a janela TestarImagemSelecionada"""

    def __init__(self, nome_projeto, arquivo_cascade):
        """Cria a janela testarImagemSelecionada e recebe o nome do projeto e o arquivo cascade"""

        self._nomeProjeto = nome_projeto
        self._arquivoCascade = arquivo_cascade
        self._dirPickerImagens = False

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################
        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Testar 1-3: Imagens - Selecionar Imagens | " + nome_projeto, pos=wx.DefaultPosition, size=wx.Size(350, 190), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer1 = wx.BoxSizer(wx.VERTICAL)

        self.panel1 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer2 = wx.BoxSizer(wx.VERTICAL)

        b_sizer3 = wx.BoxSizer(wx.VERTICAL)

        self.staticText1 = wx.StaticText(self.panel1, wx.ID_ANY, "Selecione o Local das imagens para realizar os testes", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText1.Wrap(-1)
        b_sizer3.Add(self.staticText1, 0, wx.ALIGN_CENTER, 0)

        b_sizer48 = wx.BoxSizer(wx.HORIZONTAL)

        self.staticText20 = wx.StaticText(self.panel1, wx.ID_ANY, "no projeto ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText20.Wrap(-1)
        b_sizer48.Add(self.staticText20, 0, wx.ALIGN_CENTER, 0)

        self.staticTextNomeProjeto = wx.StaticText(self.panel1, wx.ID_ANY, "NomeProjeto", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNomeProjeto.Wrap(-1)
        self.staticTextNomeProjeto.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer48.Add(self.staticTextNomeProjeto, 0, 0, 0)

        self.staticText22 = wx.StaticText(self.panel1, wx.ID_ANY, ".", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText22.Wrap(-1)
        b_sizer48.Add(self.staticText22, 0, 0, 0)

        b_sizer3.Add(b_sizer48, 1, wx.ALIGN_CENTER, 10)

        b_sizer2.Add(b_sizer3, 0, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 5)

        b_sizer67 = wx.BoxSizer(wx.HORIZONTAL)

        self.dirPickerImagens = wx.DirPickerCtrl(self.panel1, wx.ID_ANY, wx.EmptyString, "Selecione a pasta com as imagens", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DIR_MUST_EXIST | wx.DIRP_SMALL | wx.DIRP_USE_TEXTCTRL)
        b_sizer67.Add(self.dirPickerImagens, 1, 0, 0)

        b_sizer2.Add(b_sizer67, 0, wx.ALL | wx.EXPAND, 10)

        self.staticline3 = wx.StaticLine(self.panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        b_sizer2.Add(self.staticline3, 0, wx.BOTTOM | wx.EXPAND | wx.TOP, 5)

        b_sizer6 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer8 = wx.BoxSizer(wx.VERTICAL)

        self.buttonVoltar = wx.Button(self.panel1, wx.ID_ANY, "< Voltar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer8.Add(self.buttonVoltar, 0, wx.ALIGN_LEFT, 0)

        b_sizer6.Add(b_sizer8, 1, wx.EXPAND, 0)

        b_sizer9 = wx.BoxSizer(wx.VERTICAL)

        self.buttonAvancar = wx.Button(self.panel1, wx.ID_ANY, "Avançar >", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer9.Add(self.buttonAvancar, 0, wx.ALIGN_RIGHT, 0)

        b_sizer6.Add(b_sizer9, 1, wx.EXPAND, 0)

        b_sizer2.Add(b_sizer6, 0, wx.ALL | wx.EXPAND, 10)

        self.panel1.SetSizer(b_sizer2)
        self.panel1.Layout()
        b_sizer1.Add(self.panel1, 1, wx.EXPAND, 0)

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Globais
        ##########################################################################################

        self.staticTextNomeProjeto.SetLabel(nome_projeto)

        ##########################################################################################
        # Configura Tela
        ##########################################################################################

        self.buttonAvancar.Enable(False)

        ##########################################################################################
        # Cursor
        ##########################################################################################
        
        self.buttonVoltar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonVoltar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        
        self.buttonAvancar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonAvancar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Evento de Disparo
        ##########################################################################################

        # Chama o método _habilitar_avancar quando o usuário atribuir um valor no campo
        self.dirPickerImagens.Bind(wx.EVT_DIRPICKER_CHANGED, self._habilitar_avancar)

        # Chama o método _go_voltar para retornar a janela Testar
        self.buttonVoltar.Bind(wx.EVT_BUTTON, self._go_voltar)

        # Chama o método _go_testar_imagens
        self.buttonAvancar.Bind(wx.EVT_BUTTON, self._go_testar_imagens)

    ##########################################################################################
    # Get e Set
    ##########################################################################################
    def get_nome_projeto(self):
        return self._nomeProjeto

    def get_arquivo_cascade(self):
        return self._arquivoCascade

    def get_dir_picker_imagens(self):
        return self._dirPickerImagens

    def set_dir_picker_imagens(self, dir_picker_imagens):
        self._dirPickerImagens = dir_picker_imagens

    ##########################################################################################
    # Controle
    ##########################################################################################
    def _habilitar_avancar(self, event):
        self.buttonAvancar.Enable(True)
        self.set_dir_picker_imagens(event.GetPath())

    ##########################################################################################
    # Cursor
    ##########################################################################################
    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

    ##########################################################################################
    # Fluxo de Tela
    ##########################################################################################
    def _go_voltar(self, event):
        """Fecha a janela atual e abre a janela Testar"""
        
        from data.views.Testar import Testar
        Testar(self.get_nome_projeto(), self.get_arquivo_cascade()).Show(True)
        self.Close(True)

    def _go_testar_imagens(self, event):
        """Fecha a janela atual e abre a janela TestarImagens"""
        
        TestarImagens(self.get_nome_projeto(), self.get_arquivo_cascade(), self.get_dir_picker_imagens()).Show(True)
        self.Close(True)
