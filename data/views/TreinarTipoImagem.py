#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.views.TreinarSample import TreinarSample


class TreinarTipoImagem (wx.Frame):

    def __init__(self, nome_projeto):
        self.nomeProjeto = nome_projeto

        ##########################################################################################
        # Criar Frame
        ##########################################################################################
        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Treinamentooo 1-3: Tipo de Imagem | " + nome_projeto, pos=wx.DefaultPosition, size=wx.Size(450, 168), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer606 = wx.BoxSizer(wx.VERTICAL)

        self.panel10 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer607 = wx.BoxSizer(wx.VERTICAL)

        b_sizer608 = wx.BoxSizer(wx.VERTICAL)

        self.staticText316 = wx.StaticText(self.panel10, wx.ID_ANY, "Selecione um dos tipos de imagem para realizar o treinamento.", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText316.Wrap(-1)
        b_sizer608.Add(self.staticText316, 0, wx.ALL, 5)

        choice_filtro_choices = ["cinza", "histograma"]
        self.choiceFiltro = wx.Choice(self.panel10, wx.ID_ANY, wx.DefaultPosition, wx.Size(110, -1), choice_filtro_choices, 0)
        self.choiceFiltro.SetSelection(0)
        b_sizer608.Add(self.choiceFiltro, 0, wx.ALIGN_CENTER | wx.ALL, 5)

        b_sizer607.Add(b_sizer608, 0, wx.ALIGN_CENTER | wx.ALL, 10)

        b_sizer609 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer610 = wx.BoxSizer(wx.VERTICAL)

        self.buttonCancelar = wx.Button(self.panel10, wx.ID_ANY, "Cancelar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer610.Add(self.buttonCancelar, 0, wx.ALL, 5)

        b_sizer609.Add(b_sizer610, 1, wx.EXPAND, 5)

        b_sizer611 = wx.BoxSizer(wx.VERTICAL)

        self.buttonAvancar = wx.Button(self.panel10, wx.ID_ANY, "Avançar >", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer611.Add(self.buttonAvancar, 0, wx.ALIGN_RIGHT | wx.ALL, 5)

        b_sizer609.Add(b_sizer611, 1, wx.EXPAND, 5)

        b_sizer607.Add(b_sizer609, 0, wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT, 10)

        self.panel10.SetSizer(b_sizer607)
        self.panel10.Layout()
        b_sizer606.Add(self.panel10, 1, wx.EXPAND, 0)

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Cursor
        ##########################################################################################
        self.buttonAvancar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonAvancar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        
        self.buttonCancelar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonCancelar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de Disparo
        ##########################################################################################
        self.buttonAvancar.Bind(wx.EVT_BUTTON, self._avancar)
        self.buttonCancelar.Bind(wx.EVT_BUTTON, self._cancelar)

    ##########################################################################################
    # Cursor
    ##########################################################################################
    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

    ##########################################################################################
    # Fluxo de Tela
    ##########################################################################################
    def _cancelar(self, event):
        from data.views.Main import Main
        Main().Show(True)
        self.Close(True)

    def _avancar(self, event):
        TreinarSample(self.nomeProjeto, self.choiceFiltro.GetStringSelection()).Show()
        self.Close()
