#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path


class Locais(object):
    """Classe responsável por informar os locais (absolutos) onde estão armazenados os arquivos dentro do sistema"""

    ######################################################################################
    # Referentes a pasta Projetos
    ######################################################################################
    """Retorna a raiz onde estão armazenados TODOS os projetos"""
    @staticmethod
    def pasta_projetos():
        return os.path.join(os.path.dirname(__file__), "..", "..", "sample", "projetos")

    """Retorna a pasta raiz do projeto informado
    
       :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_projeto(self, nome_projeto):
        return os.path.join(self.pasta_projetos(), nome_projeto)

    ######################################################################################
    # Referentes a pasta Cascade
    ######################################################################################
    """Retorna a raiz da pasta 'cascade', onde estão localizados os arquivos XML de treinamento do projeto
    
        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_cascade(self, nome_projeto):
        return os.path.join(self.pasta_projeto(nome_projeto), "cascade")

    ######################################################################################
    # Referentes a pasta Teste
    ######################################################################################
    """Retorna a raiz da pasta 'teste', onde estão localizados os arquivos para teste do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_teste(self, nome_projeto):
        return os.path.join(self.pasta_projeto(nome_projeto), "teste")

    """Retorna a raiz da pasta 'teste/imagens', onde estão localizadas as imagens de testes do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_teste_imagens(self, nome_projeto):
        return os.path.join(self.pasta_teste(nome_projeto), "imagens")

    """Retorna a raiz da pasta 'teste/imagens/originais', onde estão armazenadas as imagens originais de teste do
    projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_teste_imagens_originais(self, nome_projeto):
        return os.path.join(self.pasta_teste_imagens(nome_projeto), "originais")

    """Retorna a raiz da pasta 'teste/imagens/detectadas', onde estão armazenadas as imagens que foram detectadas nos
    testes do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_teste_imagens_detectadas(self, nome_projeto):
        return os.path.join(self.pasta_teste_imagens(nome_projeto), "detectadas")

    ######################################################################################
    # Referentes a pasta Positivas
    ######################################################################################
    """Retorna a raiz da pasta 'positivas', onde estão armazenadas as imagens POSITIVAS do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_positivas(self, nome_projeto):
        return os.path.join(self.pasta_projeto(nome_projeto), "positivas")

    """Retorna a raiz da pasta 'positivas/originais', onde estão armazenadas as imagens POSITIVAS ORIGINAIS do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_positivas_originais(self, nome_projeto):
        return os.path.join(self.pasta_imagens_positivas(nome_projeto), "originais")

    """Retorna a raiz da pasta 'positivas/editadas', onde estão armazenadas as imagens POSITIVAS EDITADAS do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_positivas_editadas(self, nome_projeto):
        return os.path.join(self.pasta_imagens_positivas(nome_projeto), "editadas")

    """Retorna a raiz da pasta 'positivas/editadas/cinza', onde estão armazenadas as imagens POSITIVAS EDITADAS com
     filtro CINZA do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_positivas_editadas_cinza(self, nome_projeto):
        return os.path.join(self.pasta_imagens_positivas_editadas(nome_projeto), "cinza")

    """Retorna a raiz da pasta 'positivas/editadas/histograma', onde estão armazenadas as imagens POSITIVAS EDITADAS com
     filtro NORMALIZAÇÃO DO HISTOGRAMA do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_positivas_editadas_histograma(self, nome_projeto):
        return os.path.join(self.pasta_imagens_positivas_editadas(nome_projeto), "histograma")

    """Retorna a raiz da pasta 'positivas/editadas/clahe', onde estão armazenadas as imagens POSITIVAS EDITADAS com
     filtro NORMALIZAÇÃO POR CLAHE do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_positivas_editadas_clahe(self, nome_projeto):
        return os.path.join(self.pasta_imagens_positivas_editadas(nome_projeto), "clahe")

    ######################################################################################
    # Referentes a pasta Negativas
    ######################################################################################
    """Retorna a raiz da pasta 'negativas', onde estão armazenadas as imagens NEGATIVAS do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_negativas(self, nome_projeto):
        return os.path.join(self.pasta_projeto(nome_projeto), "negativas")

    """Retorna a raiz da pasta 'negativas/originais', onde estão armazenadas as imagens NEGATIVAS ORIGINAIS do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_negativas_originais(self, nome_projeto):
        return os.path.join(self.pasta_imagens_negativas(nome_projeto), "originais")

    """Retorna a raiz da pasta 'negativas/editadas', onde estão armazenadas as imagens NEGATIVAS EDITADAS do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_negativas_editadas(self, nome_projeto):
        return os.path.join(self.pasta_imagens_negativas(nome_projeto), "editadas")

    """Retorna a raiz da pasta 'negativas/editadas/cinza', onde estão armazenadas as imagens NEGATIVAS EDITADAS com
    filtro CINZA do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_negativas_editadas_cinza(self, nome_projeto):
        return os.path.join(self.pasta_imagens_negativas_editadas(nome_projeto), "cinza")

    """Retorna a raiz da pasta 'negativas/editadas/histograma', onde estão armazenadas as imagens NEGATIVAS EDITADAS
     com filtro NORMALIZAÇÃO DO HISTOGRAMA do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_negativas_editadas_histograma(self, nome_projeto):
        return os.path.join(self.pasta_imagens_negativas_editadas(nome_projeto), "histograma")

    """Retorna a raiz da pasta 'negativas/editadas/clahe', onde estão armazenadas as imagens NEGATIVAS EDITADAS com
     filtro NORMALIZAÇÃO POR CLAHE do projeto

        :param str nome_projeto: define qual é o nome do projeto
    """
    def pasta_imagens_negativas_editadas_clahe(self, nome_projeto):
        return os.path.join(self.pasta_imagens_negativas_editadas(nome_projeto), "clahe")
