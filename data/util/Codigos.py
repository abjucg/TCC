#!/usr/bin/env python
# -*- coding: utf-8 -*-


def verifica_codigo(codigo):
    """Converte o código recebido

        :parameter str codigo: define a cadeia de caracteres, sendo definidas como:
            p: positivas
            n: negativas
            t: teste

        :returns: positivas | negativas | teste
    """
    if codigo == "p":
        novo_codigo = "positivas"

    elif codigo == "n":
        novo_codigo = "negativas"

    elif codigo == "t":
        novo_codigo = "teste"

    else:
        return False

    return novo_codigo
